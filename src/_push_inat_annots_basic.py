#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Implements the same functionality as push_innat_annots.py, but has all
# options but the input file preconfigured.

from argparse import ArgumentParser
from annotslib.inat_annots import push_annotations


argp = ArgumentParser(
    description='Pushes image annotations back to iNaturalist.'
)
argp.add_argument(
    'annot_file', type=str, nargs='+', help='A CSV file with image '
    'annotations.'
)

args = argp.parse_args()

# Set all options.
id_col = 'id'
annot_col = 'open_flowers'
vocab = 'Plant Phenology'
skip_unmapped = True
term_map = {
    '1': 'Flowering'
}

# Process all annotations.
try:
    push_annotations(
        args.annot_file, id_col, annot_col, vocab, term_map, skip_unmapped
    )
except Exception as err:
    exit('\nERROR: {0}\n'.format(str(err)))

