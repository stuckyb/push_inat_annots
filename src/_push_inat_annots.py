#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from argparse import ArgumentParser, Action
from annotslib.inat_annots import push_annotations, get_controlled_vocabs


class ShowVocabsAction(Action):
    """
    An argparse Action class that prints all available iNaturalist annotation
    vocabularies and exits`.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        vocabs = get_controlled_vocabs()
        for vocab_name in sorted(vocabs.keys()):
            print(vocab_name)
            for term in vocabs[vocab_name]['vals'].keys():
                print('  ', term)
            print()
        parser.exit(0)


argp = ArgumentParser(
    description='Pushes image annotations back to iNaturalist.'
)
argp.add_argument(
    '-d', '--id_col', type=str, required=True,
    help='The column of the annotations CSV file(s) that contains iNaturalist '
    'observation IDs.'
)
argp.add_argument(
    '-a', '--annot_col', type=str, required=True,
    help='The column of the annotations CSV file(s) that contains the '
    'annotation values.'
)
argp.add_argument(
    '-b', '--vocab', type=str, required=True,
    help='The iNaturalist controlled vocabulary to use for annotations.'
)
argp.add_argument(
    '-m', '--term_mapping', type=str, required=True, help='A JSON object that '
    'defines how annotation values in the input CSV file(s) should be mapped '
    'to annotation values in the iNaturalist controlled vocabulary.  The '
    'format should be \'{"CSV annot. val.": "iNat annot. val."}\'.'
)
argp.add_argument(
    '-s', '--skip_unmapped', action='store_true', help='If provided, '
    'annotation records in a local CSV file with unmapped annotation values '
    'will simply be skipped rather than triggering an error.'
)
argp.add_argument(
    '--show_vocabs', nargs=0, action=ShowVocabsAction, help='Display all '
    'available iNaturalist annotation vocabularies and exit.'
)
argp.add_argument(
    'annot_file', type=str, nargs='+', help='A CSV file with image '
    'annotations.'
)

args = argp.parse_args()

# Parse the term mapping.
try:
    term_map = json.loads(args.term_mapping)
except Exception as err:
    exit(
        '\nERROR: Could not parse term mapping string. {0}\n'.format(str(err))
    )

# Process all annotations.
try:
    push_annotations(
        args.annot_file, args.id_col, args.annot_col, args.vocab, term_map,
        args.skip_unmapped
    )
except Exception as err:
    exit('\nERROR: {0}\n'.format(str(err)))

