# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import csv
import os.path
import json
import time
from contextlib import ExitStack
import platform
from .oauth2_pkce import getJWT


API_URL = 'https://api.inaturalist.org/v1/'
AUTH_URL = 'https://www.inaturalist.org/'
APP_ID = 'c636f4be7d3ead704479c0c79209e17b95a727c4df8255c6bb1613e6140cc11a'

def get_controlled_vocabs():
    """
    Retrieves the iNaturalist controlled vocabularies used for annotations.
    """
    resp = requests.get(API_URL + 'controlled_terms')
    res = resp.json()

    vocab = {}

    for result in res['results']:
        vocab_name = result['label']
        vocab[vocab_name] = {}
        vocab[vocab_name]['id'] = result['id']
        vocab[vocab_name]['vals'] = {}

        for val in result['values']:
            vocab[vocab_name]['vals'][val['label']] = val['id']

    return vocab

def _verify_annot_files(annot_files, id_col, annot_col):
    """
    Verifies that each local annotation file exists and includes the required
    CSV columns.
    """
    req_cols = [id_col, annot_col]

    for fpath in annot_files:
        if not(os.path.isfile(fpath)):
            raise Exception(
                'The annotations file {0} could not be found.'.format(fpath)
            )

        with open(fpath) as fin:
            reader = csv.reader(fin)
            colnames = reader.__next__()
            for colname in req_cols:
                if colname not in colnames:
                    raise Exception(
                        'The annotations file {0} does not include the '
                        'required column "{1}".'.format(fpath, colname)
                    )

def _check_inat_vocab(vocabs, vocab_name, term_map):
    """
    Checks that a specified iNaturalist controlled vocabulary exists and
    contains the terms specified in a term mapping dictionary.
    """
    if vocab_name not in vocabs:
        raise Exception(
            'The iNaturalist controlled vocabulary "{0}" could not be '
            'found.'.format(vocab_name)
        )

    for key, val in term_map.items():
        if val not in vocabs[vocab_name]['vals']:
            raise Exception(
                'The term "{0}" was not found in the iNaturalist controlled '
                'vocabulary "{1}".'.format(val, vocab_name)
            )

def _getFailLogPath(input_fpath):
    """
    Returns a path to use for a new failure logging file.  The new path is
    guaranteed not to exist.
    """
    dirname = os.path.dirname(input_fpath)
    parts = os.path.splitext(os.path.basename(input_fpath))

    if platform.system() == 'Windows':
        # ':' characters cannot be used in file names on Windows.
        new_fname_base = '{0}-fail_log-{1}'.format(
            parts[0], time.strftime('%Y%b%d-%H_%M_%S', time.localtime())
        )
    else:
        new_fname_base = '{0}-fail_log-{1}'.format(
            parts[0], time.strftime('%Y%b%d-%H:%M:%S', time.localtime())
        )

    new_fpath = os.path.join(dirname, new_fname_base + parts[1])

    cnt = 1
    while os.path.exists(new_fpath):
        new_fpath = os.path.join(
            dirname, '{0}-{1}'.format(new_fname_base, cnt) + parts[1]
        )
        cnt += 1
        
    return new_fpath

def push_annotations(
    annot_files, id_col, annot_col, vocab, term_map, skip_unmapped=False
):
    """
    annot_files: A list of local annotation file paths.
    id_col: The CSV column containing iNaturalist observation IDs.
    annot_col: The CSV column containing annotation values.
    vocab: The name of the iNaturalist annotation vocabulary to use.
    term_map: A dictionary mapping CSV annotation values to terms in the
        iNaturalist annotation vocabulary.
    skip_unmapped: If True, local annotation values without a term mapping will
        be skipped without triggering an exception.
    """
    vocabs = get_controlled_vocabs()
    #print(vocabs)
    _check_inat_vocab(vocabs, vocab, term_map)

    # Before pushing any annotations to iNaturalist, verify that the
    # annotations files exist and include the required columns.
    _verify_annot_files(annot_files, id_col, annot_col)

    # Get a JWT.
    jwt = getJWT(AUTH_URL, APP_ID)

    # Process the annotations.
    annot_cnt = 0
    for fpath in annot_files:
        print('Processing file {0}...'.format(fpath))

        flog_path = _getFailLogPath(fpath)

        with ExitStack() as cstack:
            fin = cstack.enter_context(open(fpath, newline=''))
            fout = cstack.enter_context(open(flog_path, 'w', newline=''))

            reader = csv.DictReader(fin)
            writer = csv.DictWriter(
                fout, [id_col, annot_col, 'status_code', 'err_txt']
            )
            writer.writeheader()
            frow = {}

            for row in reader:
                obs_id = row[id_col]
                annot_val = row[annot_col]
                if obs_id == '' or annot_val == '':
                    continue

                annot_cnt += 1

                if annot_val not in term_map:
                    if not(skip_unmapped):
                        raise Exception(
                            'No mapping was provided for the annotation value '
                            '"{0}".'.format(annot_val)
                        )
                    else:
                        print(
                            '  No mapping for annotation value "{0}"; '
                            'skipping...'.format(annot_val)
                        )
                        continue

                annot_val_id = vocabs[vocab]['vals'][term_map[annot_val]]

                payload = {
                    'annotation': {
                        'resource_type': 'Observation',
                        'resource_id': int(obs_id),
                        'controlled_attribute_id': vocabs[vocab]['id'],
                        'controlled_value_id': annot_val_id
                    }
                }

                resp = requests.post(
                    API_URL + 'annotations',
                    headers={
                        'Authorization': jwt
                    },
                    data=json.dumps(payload)
                )
                if resp.status_code != 200:
                    print(
                        '  WARNING: Error when pushing annotation for {0}:\n    '
                        'Status code: {1}\n    Full response: "{2}"'.format(
                            obs_id, resp.status_code, resp.text
                        )
                    )
                    frow[id_col] = obs_id
                    frow[annot_col] = annot_val
                    frow['status_code'] = resp.status_code
                    frow['err_txt'] = resp.text
                    writer.writerow(frow)

                if annot_cnt % 20 == 0:
                    print('  Annotations processed: {0}'.format(annot_cnt))

                # To avoid API usage limits, limit requests to ~60 per minute.
                time.sleep(1.0)

        print('  Total annotations processed: {0}'.format(annot_cnt))

