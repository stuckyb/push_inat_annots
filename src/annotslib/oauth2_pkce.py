# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Implements a complete, self-contained OAuth2 PKCE authentication flow that
# does not require any external HTTP resources beyond the authentication HTTP
# server.  Authentication callbacks are handled via a temporary HTTP server
# bound to localhost that handles the authorization code redirect.

import secrets
import hashlib
import base64
import urllib.parse as urlparse
import http.server as hs
import socketserver
import re
import requests
import subprocess as sp
import platform


REDIRECT_URL = 'http://127.0.0.1:8080/clientauth'


class AuthResponseServer(socketserver.ThreadingMixIn, hs.HTTPServer):
    """
    A custom HTTP server for handling OAuth2 PKCE authentication flow
    callbacks.  After handling a redirect and obtaining an authorization code,
    the server automatically terminates.  Although Python 3.7 includes
    ThreadingHTPServer, we instead subclass HTTPServer with the threading mixin
    to ensure the code will be compatible with earlier versions of Python 3.
    """
    def setAuthCode(self, auth_code):
        self.auth_code = auth_code

    def getAuthCode(self):
        return self.auth_code

class AuthResponseHandler(hs.BaseHTTPRequestHandler):
    def do_GET(self):
        path_str = urlparse.unquote(self.path)

        if path_str.find('code=') == -1:
            self.server.setAuthCode(None)
            pg_title = 'Authorization failed'
            pg_msg = ('Authorization failed. The request URL did not include an '
                'authorization code.')
        else:
            auth_code = path_str.split('code=')[1]
            self.server.setAuthCode(auth_code)
            pg_title = 'Authorization succeeded'
            pg_msg = 'Authorization succeeded. You may now close this page.'

        resp_txt = """<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
<head>
   <title>{0}</title>
</head>
<body>
   <p>{1}</p>
</body>
</html>"""

        self.send_response(200)
        self.send_header('Content-Type:', 'text/html')
        self.end_headers()
        self.wfile.write(resp_txt.format(pg_title, pg_msg).encode('utf-8'))

        # This is safe to call here because it will be running outside of the
        # main thread.
        self.server.shutdown()

    def log_message(self, format, *args):
        pass

def createCodePair():
    """
    Creates a random code verifier and SHA-256 digest pair.  Both members of
    the pair will be URL-safe.
    """
    # Create a code verifier that is ~64 bytes long.
    code_v = secrets.token_urlsafe(50)
    code_v = re.sub(r'[^a-zA-Z0-9]', '', code_v)

    code_c = hashlib.sha256(code_v.encode('utf-8')).digest()
    code_c = base64.urlsafe_b64encode(code_c).decode('utf-8')
    code_c = code_c.replace('=', '')

    return (code_v, code_c)

def getJWT(auth_url, client_id):
    """
    Implements the OAuth2 PKCE authentication flow, obtains a server-provided
    access token upon successful authentication, and uses the access token to
    obtain a JSON Web Token, which is the return value.
    """
    code_v, code_c = createCodePair()

    query_str = urlparse.urlencode({
        'client_id': client_id,
        'redirect_uri': REDIRECT_URL,
        'response_type': 'code',
        'code_challenge_method': 'S256', 
        'code_challenge': code_c
    })
    user_auth_url = auth_url + 'oauth/authorize?' + query_str

    # Display the authentication page for the user, if possible.
    sysname = platform.system()
    if sysname == 'Linux':
        sp.Popen(['xdg-open', user_auth_url], stdout=sp.DEVNULL)
    elif sysname == 'Windows':
        # Running command prompt commands (like "start") does not work if the
        # arguments are passed to Popen() as a list.
        cmd_str = 'start "" "{0}"'.format(user_auth_url)
        sp.Popen(cmd_str, shell=True, stdout=sp.DEVNULL)
    else:
        print('Please open this URL in a web browser: {0}'.format(
            user_auth_url
        ))

    httpd = AuthResponseServer(('127.0.0.1', 8080), AuthResponseHandler)

    # This will block until the server shuts down, which will be initiated by
    # the GET request handler thread.
    httpd.serve_forever()

    auth_code = httpd.getAuthCode()
    if auth_code is None:
        raise Exception('Unable to obtain authorization code.')

    # Get the access token.
    resp = requests.post(
        auth_url + 'oauth/token',
        data={
            'client_id': client_id,
            'code': auth_code,
            'redirect_uri': REDIRECT_URL,
            'grant_type': 'authorization_code',
            'code_verifier': code_v
        }
    )
    #print(resp.status_code)
    #print(resp.headers)
    json_r = resp.json()
    if 'access_token' not in json_r:
        raise Exception(
            'Access token request failed.  Full server response: '
            '"{0}".'.format(resp.text)
        )

    # Use the access token to obtain a JWT.
    resp = requests.get(
        auth_url + 'users/api_token',
        headers={
            'Authorization': 'Bearer {0}'.format(json_r['access_token'])
        }
    )
    json_r = resp.json()
    if 'api_token' not in json_r:
        raise Exception(
            'JWT request failed.  Full server response: '
            '"{0}".'.format(resp.text)
        )

    return json_r['api_token']

