
# Building the push_inat_annots executable for Windows

To build the executable, you will need to have PyInstaller and ensure that all
of push_inat_annots' required libraries are available in the build environment.
Then, run
```
pyinstaller push_inat_annots.spec
```

That should create an updated `push_inat_annots.exe` in the `dist` folder.


## Creating a new spec file

The pyinstaller spec file, `push_inat_annots.spec` can be created with this
command:
```
pyinstaller -F -n push_inat_annots  ..\..\src\_push_inat_annots_basic.py
```

The command above will create a spec file for the pre-configured version of
`push_inat_annots`.  To create an executable that includes the full CLI, run
```
pyinstaller -F -n push_inat_annots  ..\..\src\_push_inat_annots.py
```

After creating a new spec file, edit the spec file and change the generated
value of the `pathex` parameter of the `Analysis` constructor to `None`.

